## Le Figaro (devops-nm@lefigaro.fr)

.DEFAULT_GOAL := help

.PHONY: help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?## .*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[36m##/[33m/'

# Variables
DOCKER_COMPOSE      = docker-compose
PHP                 = $(DOCKER_COMPOSE) run --rm php-tools
COMPOSER            = $(PHP) php -d memory_limit=3G /usr/bin/composer

JENKINS_DOCKER   = $(DOCKER_COMPOSE) -f docker-compose.jenkins.yml -H=$(HOST):$(PORT) $(TLS_PARAMS)
JENKINS_PHP      = $(JENKINS_DOCKER) run --rm php-tools
JENKINS_COMPOSER = $(JENKINS_PHP) composer

##
## Setup
## -----
.PHONY: build kill install reset start stop clean restart uninstall

build:
	@$(DOCKER_COMPOSE) pull

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

install: clean build start vendor ## Install and start the project

reset: uninstall install ## Stop and start a fresh install of the project

start: ## Starts the docker containers
	$(DOCKER_COMPOSE) up -d --remove-orphans nginx

stop: ## Stops the docker containers
	$(DOCKER_COMPOSE) stop

clean: kill ## Stop the project and remove generated files
	rm -rf vendor

restart: stop start ## Restarts the docker containers

uninstall: stop clean ## Uninstalls the project

composer.lock: composer.json
	$(COMPOSER) update --no-scripts --no-interaction

vendor: composer.lock
	$(COMPOSER) install

##
## Tools
## -----

.PHONY: composer-install satis-build php-tools

composer-install: ## Runs composer install
	$(COMPOSER) install

satis-build: ## Build satis
	$(PHP) bin/satis build satis.json

php-tools: ## Starts the php-tools container
	$(PHP)

#
# Jenkins
# -------
#
.PHONY: jenkins-initialize jenkins-clean

jenkins-initialize: .env.local
	sed -i 's/APP_ENV=dev/APP_ENV=test/g' .env.local
	$(JENKINS_DOCKER) down -v --remove-orphans
	$(JENKINS_DOCKER) pull
	$(JENKINS_DOCKER) up -d --remove-orphans nginx
	$(JENKINS_COMPOSER) install --no-interaction

jenkins-clean:
	if [ -d "vendor" ]; then rm -rf vendor; fi
	$(JENKINS_DOCKER) down --remove-orphans