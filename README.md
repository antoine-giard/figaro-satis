# Figaro Satis Repository

Simple static Composer repository for Le Figaro.

## Install

`make install`

Then build the packages.  
The initial build is long as it creates a tar for each tag/branch from packages listed in satis.json.  

`make satis-build`

Make sure that you added to your hosts: 
`satis.dev.lefigaro.fr` 
and the satis ui should be available from 
`https://satis.dev.lefigaro.fr:10613/`
  
To use the satis in your project, just add to your composer.json:
```
...
"repositories": {
     "satis": {
       "type": "composer",
       "url": "https://satis.dev.lefigaro.fr:10613/"
     }
   }
``` 